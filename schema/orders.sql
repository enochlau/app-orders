BEGIN;

CREATE TABLE IF NOT EXISTS orders (
  id UUID DEFAULT uuid_generate_v1() PRIMARY KEY,
  data JSONB NOT NULL DEFAULT '{}' :: JSONB,
  status VARCHAR(255) DEFAULT 'created',
  created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
  modified_at TIMESTAMPTZ NOT NULL DEFAULT now(),
  deleted_at TIMESTAMPTZ
);

COMMIT;