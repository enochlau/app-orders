const client = require('../config/environment');

async function getOrders() {
  await client.connect();
  const query = `
    SELECT * FROM orders
    WHERE deleted_at IS NULL
    ORDER BY created_at DESC
  `;

  try {
    const result = await client.query(query);

    return result.rows;
  } catch (err) {
    console.log(err);
    await client.end();
    return null;
  }
}

async function getOrderById(id: string, client) {
  const query = `
    SELECT * FROM orders WHERE id = $1
  `;

  try {
    const result = await client.query(query, [id]);

    return result.rows[0];
  } catch (err) {
    console.log(err);
    await client.end();
    return null;
  }
}

async function insertOrder(payload, client) {
  const query = `
    INSERT INTO orders (data, status, created_at, modified_at)
    VALUES ($1, $2, now(), now())
    RETURNING *
  `;

  try {
    const result = await client.query(query, payload);

    return result.rows[0];
  } catch (err) {
    console.log(err);
    return null;
  }
}

async function updateOrderStatus(id, status, client) {
  const query = `
    UPDATE orders SET status = $2
    WHERE id = $1
    RETURNING *
  `;

  try {
    const result = await client.query(query, [id, status]);

    return result.rows[0];
  } catch (err) {
    console.log(err);
    return null;
  }
}

async function deleteOrder(id) {
  await client.connect();
  const query = `
    UPDATE orders
    SET deleted_at = NOW()
    WHERE id = $1
    RETURNING *
  `;

  try {
    const result = await client.query(query, [id]);
    await client.end();
    return result.rows[0];
  } catch (err) {
    console.log(err);
    await client.end();
    return null;
  }
}

export { getOrders, getOrderById, insertOrder, deleteOrder, updateOrderStatus };
