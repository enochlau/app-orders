import fetch from 'node-fetch';

async function makePayment(orderId: string) {
  const result = await fetch(`${process.env.PAYMENTS_API_URL}/payments`, {
    method: 'post',
    body: JSON.stringify({ order_id: orderId }),
    headers: { 'Content-Type': 'application/json' },
  });
  const body = await result.json();

  return body;
}

export { makePayment };
