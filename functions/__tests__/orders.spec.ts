import { Client } from 'pg';
import * as OrderFunctions from '../orders';

jest.mock('pg', () => {
  const mClient = {
    connect: jest.fn(),
    query: jest.fn(),
    end: jest.fn(),
  };
  return { Client: jest.fn(() => mClient) };
});

describe('orders functions', () => {
  let client;
  beforeEach(() => {
    client = new Client();
  });
  afterEach(() => {
    jest.clearAllMocks();
  });
  it('should get orders correctly', async () => {
    client.query.mockResolvedValueOnce({ rows: [], rowCount: 0 });
    await OrderFunctions.getOrders();
    expect(client.connect).toBeCalledTimes(1);
    expect(client.query).toHaveBeenCalled();
    expect(client.end).toBeCalledTimes(1);
  });
  it('should insert orders correctly', async () => {
    const data = [
      {
        details: 'test details',
      },
      'created',
    ];
    client.query.mockResolvedValueOnce({ rows: [], rowCount: 0 });
    await OrderFunctions.insertOrder(data);
    expect(client.connect).toBeCalledTimes(1);
    expect(client.query).toHaveBeenCalledWith(
      `
    INSERT INTO orders (data, status, created_at, modified_at)
    VALUES ($1, $2, now(), now())
    RETURNING *
  `,
      data,
    );
    expect(client.end).toBeCalledTimes(1);
  });
  it('should delete orders correctly', async () => {
    const id = 'testId';
    client.query.mockResolvedValueOnce({ rows: [], rowCount: 0 });
    await OrderFunctions.deleteOrder(id);
    expect(client.connect).toBeCalledTimes(1);
    expect(client.query).toHaveBeenCalledWith(
      `
    UPDATE orders
    SET deleted_at = NOW()
    WHERE id = $1
    RETURNING *
  `,
      [id],
    );
    expect(client.end).toBeCalledTimes(1);
  });
});
