import * as AWS from 'aws-sdk';

const sqs = new AWS.SQS({
  apiVersion: '2012-11-05',
  endpoint: 'http://localhost:9324',
  region: 'ap-southeast-1',
});

function sendMessage(payload) {
  const params = {
    DelaySeconds: 5,
    MessageBody: JSON.stringify(payload),
    QueueUrl: process.env.SQS_URL,
  };

  const result = sqs.sendMessage(params).promise();
  return result;
}

export { sendMessage };
