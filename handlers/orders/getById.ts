'use strict';

import middy from '@middy/core';
import warmup from '@middy/warmup';
import jsonBodyParser from '@middy/http-json-body-parser';
import httpErrorHandler from '@middy/http-error-handler';
import httpSecurityHeaders from '@middy/http-security-headers';
import validator from '@middy/validator';
import { getOrderById } from '../../functions/orders';
import { createResponse } from '../../tools/response';
const client = require('../../config/environment');

const getById = async (event) => {
  const { id } = event.pathParameters;
  await client.connect();
  const order = await getOrderById(id, client);
  await client.end();

  return createResponse({ order });
};

const inputSchema = {
  properties: {},
};

export const handler = middy(getById)
  .use(warmup())
  .use(jsonBodyParser())
  .use(validator({ inputSchema }))
  .use(httpSecurityHeaders())
  .use(httpErrorHandler());
