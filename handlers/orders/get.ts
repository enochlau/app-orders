'use strict';

import middy from '@middy/core';
import warmup from '@middy/warmup';
import jsonBodyParser from '@middy/http-json-body-parser';
import httpErrorHandler from '@middy/http-error-handler';
import httpSecurityHeaders from '@middy/http-security-headers';
import validator from '@middy/validator';
import { getOrders } from '../../functions/orders';
import { createResponse } from '../../tools/response';

const get = async () => {
  const orders = await getOrders();

  return createResponse({ orders });
};

const inputSchema = {
  properties: {},
};

export const handler = middy(get)
  .use(warmup())
  .use(jsonBodyParser())
  .use(validator({ inputSchema }))
  .use(httpSecurityHeaders())
  .use(httpErrorHandler());
