'use strict';

import middy from '@middy/core';
import warmup from '@middy/warmup';
import jsonBodyParser from '@middy/http-json-body-parser';
import httpErrorHandler from '@middy/http-error-handler';
import httpSecurityHeaders from '@middy/http-security-headers';
import validator from '@middy/validator';
import { insertOrder, updateOrderStatus } from '../../functions/orders';
import { createResponse } from '../../tools/response';
import { makePayment } from '../../functions/payments';
import { sendMessage } from '../../functions/sqs';
const client = require('../../config/environment');

const post = async (event) => {
  const { body } = event;
  await client.connect();

  const result = await insertOrder(
    [{ details: body.details }, 'created'],
    client,
  );
  if (!result) {
    return createResponse({ errorMessage: 'Error creating order' });
  }
  // make payment
  const paymentResult = await makePayment(result.id);
  if (!paymentResult || !paymentResult.transaction_id) {
    await updateOrderStatus(result.id, 'declined', client);
    return createResponse({ errorMessage: 'Payment declined' });
  }

  await updateOrderStatus(result.id, 'confirmed', client);

  await sendMessage({ order_id: result.id });

  await client.end();

  return createResponse({ order: result });
};

const inputSchema = {
  required: ['body'],
  properties: {
    body: {
      details: {
        type: 'string',
      },
    },
  },
};

export const handler = middy(post)
  .use(warmup())
  .use(jsonBodyParser())
  .use(validator({ inputSchema }))
  .use(httpSecurityHeaders())
  .use(httpErrorHandler());
