'use strict';

import middy from '@middy/core';
import warmup from '@middy/warmup';
import jsonBodyParser from '@middy/http-json-body-parser';
import httpErrorHandler from '@middy/http-error-handler';
import httpSecurityHeaders from '@middy/http-security-headers';
import validator from '@middy/validator';
import { deleteOrder } from '../../functions/orders';
import { createResponse } from '../../tools/response';

const del = async (event) => {
  const {
    pathParameters: { id },
  } = event;

  const result = await deleteOrder(id);

  return createResponse({ order: result });
};

const inputSchema = {
  properties: {
    pathParameters: {
      type: 'object',
      required: ['id'],
      properties: {
        id: {
          type: 'string',
        },
      },
    },
  },
};

export const handler = middy(del)
  .use(warmup())
  .use(jsonBodyParser())
  .use(validator({ inputSchema }))
  .use(httpSecurityHeaders())
  .use(httpErrorHandler());
