'use strict';

import middy from '@middy/core';
import warmup from '@middy/warmup';
import jsonBodyParser from '@middy/http-json-body-parser';
import httpErrorHandler from '@middy/http-error-handler';
import httpSecurityHeaders from '@middy/http-security-headers';
import validator from '@middy/validator';
import { updateOrderStatus } from '../../functions/orders';
import { createResponse } from '../../tools/response';
const client = require('../../config/environment');

const post = async (event) => {
  const { body } = event;
  const { order_id } = body;

  await client.connect();
  const result = updateOrderStatus(order_id, 'delivered', client);
  await client.end();

  return createResponse({ result: result });
};

const inputSchema = {};

export const handler = middy(post)
  .use(warmup())
  .use(jsonBodyParser())
  .use(validator({ inputSchema }))
  .use(httpSecurityHeaders())
  .use(httpErrorHandler());
