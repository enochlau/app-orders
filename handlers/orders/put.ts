'use strict';

import middy from '@middy/core';
import warmup from '@middy/warmup';
import jsonBodyParser from '@middy/http-json-body-parser';
import httpErrorHandler from '@middy/http-error-handler';
import httpSecurityHeaders from '@middy/http-security-headers';
import validator from '@middy/validator';
import { updateOrderStatus } from '../../functions/orders';
import { createResponse } from '../../tools/response';
const client = require('../../config/environment');

const put = async (event) => {
  const { body } = event;
  const { id } = event.pathParameters;

  await client.connect();
  const result = await updateOrderStatus(id, body.status, client);
  await client.end();

  return createResponse({ order: result });
};

const inputSchema = {
  required: ['body'],
  properties: {
    body: {
      status: {
        type: 'string',
      },
    },
  },
};

export const handler = middy(put)
  .use(warmup())
  .use(jsonBodyParser())
  .use(validator({ inputSchema }))
  .use(httpSecurityHeaders())
  .use(httpErrorHandler());
