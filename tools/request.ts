export const createHeaders = () => {
  return {
    'x-api-key': process.env.API_KEY,
    'x-application-version': process.env.APPLICATION_VERSION,
    'Content-Type': 'application/json',
  };
};
